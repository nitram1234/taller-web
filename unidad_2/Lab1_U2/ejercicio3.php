<!DOCTYPE html>
<html class="no-js" lang="es">

<head>
	<title>Ejercicio 3</title>
	<meta charset="utf-8">
	<link rel="stylesheet" href="diseño.css">
</head>

<h1>Tabla Configurable</h1>
<body>

	<!--Cuadros para ingreso de texto (inputs)-->
	<FORM METHOD="GET" ACTION="">
		<LABEL for="ancho">Longitud de la tabla:</label>
		<INPUT type="text" name="ancho"><br><br>

		<label for="color">Color en inglés:</label>
		<INPUT type="text" name="color"><br><br>

		<INPUT type="submit" value="Intro">
	</FORM>

	<!-- <FORM METHOD="GET" ACTION="">
		<LABEL for="ancho">Longitud de la tabla:</label>
		<INPUT type="text" name="ancho"><br><br>

		<INPUT type="submit" value="Intro">
	</FORM>
	
	<FORM METHOD="POST" ACTION="">
		<label for="color">Color en inglés:</label>
		<INPUT type="text" name="color"><br><br>

		<INPUT type="submit" value="Intro">
	</FORM> -->

	<table>
	<?php
		//variables para configurar tabla:
		$ancho = $_GET["ancho"]-1;
		$color = $_GET["color"];
		$indice=0; 
		 
		//doble ciclo, para formar matriz de ancho x ancho:
		for ($fila = 0;$fila <= $ancho; $fila++ ) {
			//echo ("<tr>");
			if($fila%2){
				echo("<tr style='background-color: $color;'>"); // SI la fila es divisible por 2: pintar fondo de $color y pasar a siguiente fila
			}else{
				echo ("<tr>");                                //pasar a siguiente fila
			}
			for ($columna = 0;$columna <= $ancho; $columna++ ) {
				$indice = $indice+1;                          //indice que aumenta con las columnas y se imprime en cada cuadro
				echo("<td>");                                 //se salta de columna
				echo("$indice");                              //se imprime indice
				echo ("</td>");
			}
			echo ("</tr>\n");
		}
		?>	
	</table>
</body>

</html>
