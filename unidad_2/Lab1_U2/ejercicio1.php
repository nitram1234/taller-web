<!DOCTYPE html>
<html class="no-js" lang="es">

<head>                                              <!-- este es el encabezado -->
	<title>Ejercicio 1</title>                        <!-- título de la pestaña -->
  <meta charset="utf-8">                            <!-- internalización de idioma -->
	<link rel="stylesheet" href="diseño.css">         <!-- referencia al diseño -->
</head>

  <body>
    <b>TABLA DEL 1 AL 100</b>                       <!-- título del cuerpo -->
      <table>
      <?php
        //variables:
        $ancho=9;
        $indice=0;

        for ($fila = 0;$fila <= $ancho; $fila++ ) {               //ciclo for que define las filas de la tabla
          echo ("<tr>"); 

          for ($columna = 0;$columna <= $ancho; $columna++ ) {    //ciclo for que define las columnas de la tabla
            $indice = $indice+1;                                  //índice que contea desde el 1 al 100 y se va imprimiendo en la tabla
            echo ("<td>"); 
            echo ("$indice");
            echo ("</td>"); 

          }
          echo ("</tr>\n");
        }
      ?>
      </table>
  </body>
 </html>