<!DOCTYPE html>
<html class="no-js" lang="es">

<head>                                              <!-- este es el encabezado -->
	<title>Ejercicio 2</title>                        <!-- título de la pestaña -->
  <meta charset="utf-8">                            <!-- internalización de idioma -->
	<link rel="stylesheet" href="diseño.css">         <!-- referencia al diseño -->
</head>

  <body>
    <h1>TABLA DE 1 A NxN</h1>                       <!-- título del cuerpo -->
      <table border="1">
      <?php

                                                          //variables:
        $ancho=16;
        $indice=0;

                                                          //doble ciclo, para formar matriz de ancho x ancho:
        for ($fila = 0;$fila <= $ancho; $fila++ ) {
          //echo ("<tr>");
          if($fila%2){
            echo("<tr style='background-color: Gray;'>"); // SI la fila es divisible por 2: pintar fondo de gris y pasar a siguiente fila
          }else{
            echo ("<tr>");                                //pasar a siguiente fila
          }
          for ($columna = 0;$columna <= $ancho; $columna++ ) {
            $indice = $indice+1;                          //indice que aumenta con las columnas y se imprime en cada cuadro
            echo("<td>");                                 //se salta de columna
            echo("$indice");                              //se imprime indice
            // if($columna%2){
            //   echo("<td style='background-color: Gray;'>");
            //   echo("$indice");
            // }else{
            //   echo("<td>");
            //   echo("$indice");
				    // }
            echo ("</td>");
          }
          echo ("</tr>\n");
        }

      ?>
      </table>
  </body>
 </html>