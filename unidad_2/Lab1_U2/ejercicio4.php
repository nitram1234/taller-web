<!DOCTYPE html>
<html class="no-js" lang="es">
<head>
	<title>Ejercicio 4</title>
    <meta charset="utf-8">
	<link rel="stylesheet" href="diseño.css">
</head>
<h1>Imágenes ordenadas</h1>
<body>
	<table>		
    <?php
        //Se lee la carpeta de fotos:
        $carpeta="fotos/";
        $carpeta_abierta = opendir($carpeta);

        $total_imagenes = count(glob($carpeta.'{*.jpg,*.gif,*.png}',GLOB_BRACE));
        //echo 'total_imagenes = '.$total_imagenes;
        $cant_filas=floor($total_imagenes / 4);

        echo($cant_filas);
        // echo($carpeta_abierta);
        echo("<tr>");

        if($total_imagenes <= 4){                                       //If para hacer sólo las 4 columnas para las 4 fotos.
            for ($i = 0;$i <= 3; $i++ ) {
                $indice_archivo = readdir($carpeta_abierta);
                echo("<td>");
                                                                        //Se especifica el formato a trabajar de los archivos:
                if(strpos($indice_archivo,'jpg') || strpos($indice_archivo,'png')){
                    $imagen = $carpeta.$indice_archivo;
                                                                        //Se printea imagen en la tabla:
                    echo'<img src='.$imagen. ' width=400px height=220px >';
                }
                echo("</td>");
            }
        }else{                                                          //ELSE para hacer más filas si es que hay más de 4 fotos.
            for ($fila = 0;$fila <= $cant_filas; $fila++ ) {
                //echo ("<tr>");
                echo ("<tr>");
                for ($i = 0;$i <= 3; $i++ ) {
                    $indice_archivo = readdir($carpeta_abierta);
                    echo("<td>");
                                                                        //Se especifica el formato a trabajar de los archivos:
                    if(strpos($indice_archivo,'jpg') || strpos($indice_archivo,'png')){
                        $imagen = $carpeta.$indice_archivo;
                                                                        //Se printea imagen en la tabla:
                        echo'<img src='.$imagen. ' width=400px height=220px >';
                    }
                    echo("</td>");
                }
                echo ("</tr>\n");
            }
        }

        // for ($i = 0;$i <= 3; $i++ ) {
        //     $indice_archivo = readdir($carpeta_abierta);
        //     echo("<td>");
        //     //Se especifica el formato a trabajar de los archivos:
        //     if ((strpos($indice_archivo,'jpg')) || (strpos($indice_archivo,'png'))){
        //         $imagen = $carpeta. $indice_archivo;
        //         //Se printea imagen en la tabla:
        //         echo'<img src='.$imagen. ' width=400px height=220px >';
        //     }
        //     echo("</td>");
        // }

        //Se cierra la carpeta:
        $carpeta_abierta->close();
        echo("</tr>");
	?>
	</table>
</body>

</html>
